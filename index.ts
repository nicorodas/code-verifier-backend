import express, { Express,Request,Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define the first route of APP
app.get("/hello", (req: Request, res: Response) => {
    // Send hello world
    res.send("Welcome to Get Route: Hello World!");
});



// Execute APP and Listen Request to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});